data: data/ruby.json

parser: node_modules/.bin/pegjs src/org-parser.js

data/%.json: tables/%.org bin/table-to-json
	bin/table-to-json --camel-case -f $< -o $@

node_modules/.bin/pegjs: package.json package-lock.json
	npm install

src/org-parser.js: src/org.pegjs
	node_modules/.bin/pegjs --allowed-start-rules start,startLine -o $@ $<
