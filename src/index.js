import {h, Component, render, createRef} from 'preact'
import {useState, useCallback} from 'preact/hooks'
import htm from 'htm'
import iassign from 'immutable-assign'
import org from './org-parser'
import {linksForStructure, linkForAPI} from './links'
import {drag, allowDrop} from './dragDrop'
import ruby from '../data/ruby.json'
const html = htm.bind(h)

function activeToggle() {
  const [active, setActive] = useState(false)
  const activate = useCallback(() => setActive(true), [active])
  const deactivate = useCallback(() => setActive(false), [active])
  return {active, activate, deactivate}
}

function AddColumnWidget({hiddenColumns, addColumn}) {
  const {active, activate, deactivate} = activeToggle()
  const ref = createRef()
  const blur = ({relatedTarget}) => {
    if(!ref.current.contains(relatedTarget)) {
      deactivate()
    }
  }
  const addThenCancel = name => () => {
    addColumn(name)
    deactivate()
  }
  return html`
    <span class="add-column" ref=${ref}>
      <button title="Un-hide a column"
              class="add"
              onClick=${activate}
              onBlur=${blur}
              disabled=${hiddenColumns.length == 0} />
      <ul class="menu ${active ? 'expanded' : ''}">
        ${hiddenColumns.map(column => AddColumnListItem({column, addColumn: addThenCancel(column)}))}
        <button title="Cancel" onClick=${deactivate} class="cancel" />
      </ul>
    </span>`
}

function AddColumnListItem({column, addColumn}) {
  return html`
    <li title="Un-hide column “${column}”"
        onClick=${addColumn}>
      <input type="text" value="+ ${column}" />
    </li>`
}

function Table ({header, hiddenColumns, addColumn, children}) {
  return html`
    <div class="table-wrapper">
      <table>${header}<tbody>${children}</tbody></table>
      <${AddColumnWidget} hiddenColumns=${hiddenColumns} addColumn=${addColumn} />
    </div>`
}

function TableHead({columns, drop, del}) {
  return html`<thead>${columns.map(column => TableHeader(column, drop, del))}</thead>`
}

function TableHeader (column, drop, del) {
  const ref = createRef()
  return html`
    <th draggable=${true}
        onDragStart=${drag}
        onDragOver=${allowDrop}
        onDrop=${drop}
        ref=${ref}
        data-name=${column}
        key=${column}>
      ${column}
      <span class="handle" title="Move column" />
      <span class="delete" title="Hide column: “${column}”" onClick=${del(column)} />
    </th>`
}

function Row ({row, columns, links: allLinks}) {
  const links = linksForStructure(row['data structure'], allLinks)
  return html`<tr key=${row}>${columns.map(column => Cell({row, column, links}))}</tr>`
}

function Cell({row, column, links}) {
  return html`<td key=${column}><${OrgStyledText} links=${links} children=${row[column]} /></td>`
}

function OrgStyledText ({links, children}) {
  if(children[0] === "") {
    return ''
  }
  const line = org.parse(children, {startRule: 'startLine'})
  const maybeLink = line => {
    const link = linkForAPI(line.text, links)
    if(link !== null) {
      const {link: href, title} = link
      return html`<a href=${href} title=${title}>${OrgStyledExpr(line)}</a>`
    } else {
      return OrgStyledExpr(line)
    }
  }

  return html`<span class="org line">${line.map(maybeLink)}</span>`
}

function OrgStyledExpr ({type, text}) {
  switch (type) {
  case 'Code':
    return html`<code class="org expr">${text}</code>`
  case 'String':
    break
  default:
    console.warn(`OrgStyledExpr got unexpected type: ${type}`)
  }
  return html`<span class="org expr">${text}</span>`
}

class App extends Component {
  constructor(...args) {
    super(...args)
    this.allHeadings = ruby.dataStructures.headings;
    this.state = ruby
    if(window && window._app === undefined) window._app = this
  }

  deleteColumn(name) {
    const index = this.state.dataStructures.headings.indexOf(name)
    this.setState(iassign(this.state, s => s.dataStructures.headings, headings => {
      headings.splice(index, 1)
      return headings
    }))
  }

  addColumn(name) {
    this.setState(iassign(this.state, s => s.dataStructures.headings, headings => {
      headings.push(name)
      return headings
    }))
  }

  moveColumn(name, before) {
    const {dataStructures: {headings}} = this.state
    const nameIndex = headings.indexOf(name)
    const beforeIndex = headings.indexOf(before)
    this.setState(iassign(this.state, s => s.dataStructures.headings, headings => {
      headings.splice(nameIndex, 1)
      headings.splice(beforeIndex, 0, name)
      return headings
    }))
  }

  get hiddenColumns() {
    return this.allHeadings.filter(head => this.state.dataStructures.headings.indexOf(head) < 0)
  }

  render() {
    const {dataStructures: {headings: columns, rows}, title, links} = this.state
    const deleteColumn = name => () => this.deleteColumn(name)
    const addColumn = name => this.addColumn(name)
    const drop = event => {
      const {target, dataTransfer} = event
      event.preventDefault()
      this.moveColumn(dataTransfer.getData('text'), target.dataset.name)
    }
    return html`<div class="app-wrapper">
                  <h1>${title}</h1>
                  <${Table}
                    header=${TableHead({columns, drop, del: deleteColumn})}
                    hiddenColumns=${this.hiddenColumns}
                    addColumn=${addColumn}
                    children=${rows.map(row => Row({row, columns, links}))} />
                </div>`
  }
}

render(html`<${App} />`, document.getElementById('app'))
