export function allowDrop(event) {
  event.preventDefault()
}

export function drag({target, dataTransfer}) {
  dataTransfer.setData('text', target.dataset.name)
}
